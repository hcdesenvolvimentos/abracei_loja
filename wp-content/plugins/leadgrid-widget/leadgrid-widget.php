<?php
/*
Plugin Name: LeadGrid Success Page Widget
Description: Add LeadGrid Widget to the WooCommerce Order Success page. See WooCommerce > Integrations > 'LeadGrid Success Page Widget' for settings.
Version: 1.0.2
Author: LeadGrid
License: GPL2
*/

/**
 * Copyright (c) 2016 LeadGrid. All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * **********************************************************************
 */

// don't call the file directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * LeadGrid_WC_Widget class
 *
 * @class LeadGrid_WC_Widget The class that holds the entire LeadGrid_WC_Widget plugin
 */
class LeadGrid_WC_Widget {

    /**
     * Constructor for the LeadGrid_WC_Widget class
     *
     * Sets up all the appropriate hooks and actions
     * within our plugin.
     *
     * @uses add_action()
     * @uses add_filter()
     */
    public function __construct() {

        // Localize our plugin
        add_action( 'init', array($this, 'localization_setup') );

        // register integration
        add_filter( 'woocommerce_integrations', array($this, 'register_integration') );
    }

    /**
     * Initializes the LeadGrid_WC_Widget() class
     *
     * Checks for an existing LeadGrid_WC_Widget() instance
     * and if it doesn't find one, creates it.
     */
    public static function init() {
        static $instance = false;

        if ( !$instance ) {
            $instance = new LeadGrid_WC_Widget();
        }

        return $instance;
    }

    /**
     * Initialize plugin for localization
     *
     * @uses load_plugin_textdomain()
     */
    public function localization_setup() {
        load_plugin_textdomain( 'wc-leadgrid-widget', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    }

    /**
     * Register integration
     *
     * @param array $interations
     * @return array
     */
    function register_integration( $interations ) {

        include dirname( __FILE__ ) . '/includes/integration.php';

        $interations[] = 'LeadGrid_WC_Widget_Integration';

        return $interations;
    }

}

// LeadGrid_WC_Widget

$wc_tracking = LeadGrid_WC_Widget::init();
