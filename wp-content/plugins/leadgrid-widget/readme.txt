=== LeadGrid Success Page Widget ===
Contributors: leadgrid
Tags: ecommerce, e-commerce, commerce, woocommerce
Donate link: 
Requires at least: 3.8
Tested up to: 4.4.3
Stable tag: trunk
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Add LeadGrid Widget to the WooCommerce Order Success page. See WooCommerce > Integrations > LeadGrid Success Page Widget for settings.

== Description ==

This plugin inserts LeadGrid Widget codes on WooCommerce success page.

= Contribute =

= Author =
[LeadGrid](http://www.leadgrid.io)

== Installation ==

Extract the zip file and just drop the contents in the wp-content/plugins/ directory of your WordPress installation and then activate the Plugin from Plugins page.

= Minimum Requirements =

* WooCommerce 2.1.0
* PHP version 5.2.4 or greater
* MySQL version 5.0 or greater

== Frequently Asked Questions ==

Nothing here right now

== Screenshots ==

== Changelog ==

= 1.0.2 - 06/08/2016 =
* Minor bugs

= 1.0.1 - 05/16/2016 =
* Minor bugs

= 1.0 - 05/15/2016 =
* Initial release

== Upgrade Notice ==

N/A