<?php

/**
 * LeadGrid class
 *
 */
class LeadGrid_WC_Widget_Integration extends WC_Integration {

    function __construct() {

        $this->id = 'wc_leadgrid_widget';
        $this->method_title = __( 'LeadGrid Success Page Widget', 'wc-leadgrid-widget' );
        $this->method_description = __( 'Configuration for LeadGrid Success Page Widget.', 'wc-leadgrid-widget' );

        // Load the settings.
        $this->init_form_fields();
        $this->init_settings();

        // Save settings if the we are in the right section
        if ( isset( $_POST[ 'section' ] ) && $this->id === $_POST[ 'section' ] ) {
            add_action( 'woocommerce_update_options_integration', array($this, 'process_admin_options') );
        }

        add_action( 'wp_footer', array($this, 'widget_handler') );
        add_action( 'woocommerce_thankyou', array($this, 'thankyou_page') );
    }

    /**
     * WooCommerce settings API fields for storing our data
     *
     * @return void
     */
    function init_form_fields() {
        $this->form_fields = array(
            'siteid' => array(
                'title'       => __( 'Site ID', 'wc-leadgrid-widget' ),
                'desc_tip'    => __( 'This is the Id of your site at LeadGrid', 'wc-leadgrid-widget' ),
                'description' => __( 'This is the Id of your site at LeadGrid. To get you site Id access <a href="http://app.leadgrid.io" target="blank">http://app.leadgrid.io</a> and go to Data Integration page then select WooCommerce settings.', 'wc-leadgrid-widget' ),
                'id'          => 'siteid',
                'type'        => 'text',
            ),
            'language' => array(
                'title'       => __( 'Widget Language', 'wc-leadgrid-widget' ),
				'desc_tip'    => __( 'The language to use when displaying the widget to your customers', 'wc-leadgrid-widget' ),
                'description' => __( 'The language to use when displaying the widget to your customers. i.e. \'en\' for english, \'es\' for spanish, \'pt-br\' for brazillian portuguese. To see all supported languages visit <a href="http://www.leadgrid.io/widget-supported-languages" target="_blank">http://www.leadgrid.io/widget-supported-languages</a>.', 'wc-leadgrid-widget' ),
                'id'          => 'language',
                'type'        => 'text',
            ),
        );
    }

    /**
     * Code print handler on HEAD tag
     *
     * It prints widget code on order received page
     *
     * @uses wp_head
     * @return void
     */
    function widget_handler() {
		$widget_language = $this->get_option( 'language', 'en' );
		$site_id = $this->get_option( 'siteid', '' );
		
		if ( is_order_received_page() ) {
				echo $this->print_widget_code($widget_language, $site_id);
			}
    }

    /**
     * Prints the code
     *
     *
     * @return void
     */
    function print_widget_code($widget_language, $site_id) {
		global $wp;
		$order = wc_get_order( $wp->query_vars['order-received'] );
		
        $order_currency = $order->get_order_currency();
		$order_total = str_replace(",", "", str_replace(".", "", bcdiv($order->get_total(), 1, 2)));
        $order_number = $order->get_order_number();
		$order_email = $order->billing_email;
		$order_postcode = $order->shipping_postcode;
		$order_country = $order->shipping_country;
		
        echo"<!-- www.LeadGrid.io - Tracking Code -->\n";
		echo"<script type='text/javascript'>\n";
		echo"     var jqlg, ws;\n";
		echo"     ws = {\n";
		echo"     //Widget Settings\n";
		echo"     OrderId: '" . $order_number . "',\n";
		echo"     CustomerEmail: '" . $order_email . "',\n";
		echo"     OrderValue: '" . $order_total . "',\n";
		echo"     OrderCurrency: '" . $order_currency . "',\n";
		echo"     OrderZipCode: '" . $order_postcode . "',\n";
		echo"     OrderCountry: '" . $order_country . "',\n";
		echo"     CustomerAge: '',\n";
		echo"     CustomerGender: '',\n";
		echo"     WidgetLanguage: '" . $widget_language . "'\n";
		echo"     };\n";
		echo"     function al() {(function (d, si, w, ws, sv, ep) {\n";
    	echo"     var h = d.getElementsByTagName('head')[0], p = d.location.protocol, s, lgs; s = d.createElement('script'); s.type = 'text/javascript'; s.charset = 'utf-8'; s.async = 1; lgs = '?SiteId=' + si + '&WidgetId=' + w + '&ScriptVersion=' + sv + '&EcommercePlatformId=' + ep + '&' + Object.keys(ws).map(function (key) { return key + '=' + encodeURIComponent(ws[key]); }).join('&'); s.src = 'https://widgets.leadgrid.io/' + ws.WidgetLanguage + '/v1.1/widget/show/' + lgs; h.appendChild(s);\n";
    	echo"     })(document, '" . $site_id . "', '', ws, '1.1', '8e99a3fd-8c43-4e40-8648-30f18c45880f');} (function () { if (window.attachEvent) window.attachEvent('onload', al); else window.addEventListener('load', al, false); })();\n";
		echo"</script>\n";
    }
}
