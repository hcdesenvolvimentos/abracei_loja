﻿<?php
/* Template Name: Temporario */

?>
<head>
	<title>Abracei - Presentes Criativos</title>
    <meta content='Abracei' name='author'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<link href="//fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet" type="text/css" />
	<style>
html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;font:inherit;font-size:100%;vertical-align:baseline}html{line-height:1}ol,ul{list-style:none}table{border-collapse:collapse;border-spacing:0}caption,th,td{text-align:left;font-weight:normal;vertical-align:middle}q,blockquote{quotes:none}q:before,q:after,blockquote:before,blockquote:after{content:"";content:none}a img{border:none}article,aside,details,figcaption,figure,footer,header,hgroup,main,menu,nav,section,summary{display:block}@font-face{font-family:"WebOswald";font-style:normal;font-weight:300;src:url(http://themes.googleusercontent.com/static/fonts/oswald/v8/HqHm7BVC_nzzTui2lzQTDT8E0i7KZn-EPnyo3HZu7kw.woff) format("woff")}@font-face{font-family:"WebOswald";font-style:normal;font-weight:400;src:url(http://themes.googleusercontent.com/static/fonts/oswald/v8/-g5pDUSRgvxvOl5u-a_WHw.woff) format("woff")}@font-face{font-family:"WebOswald";font-style:normal;font-weight:700;src:url(http://themes.googleusercontent.com/static/fonts/oswald/v8/bH7276GfdCjMjApa_dkG6T8E0i7KZn-EPnyo3HZu7kw.woff) format("woff")}.wrapper{*zoom:1;max-width:466px;max-width:29.125rem;_width:466px;padding-left:6px;padding-left:0.375rem;padding-right:6px;padding-right:0.375rem;margin-left:auto;margin-right:auto;-moz-background-origin:content;-o-background-origin:content-box;-webkit-background-origin:content;background-origin:content-box;-moz-background-clip:content;-o-background-clip:content-box;-webkit-background-clip:content;background-clip:content-box}.wrapper:after{content:"";display:table;clear:both}@media (min-width: 628px){.wrapper{max-width:628px;max-width:39.25rem;-moz-background-origin:content;-o-background-origin:content-box;-webkit-background-origin:content;background-origin:content-box;-moz-background-clip:content;-o-background-clip:content-box;-webkit-background-clip:content;background-clip:content-box}}@media (min-width: 952px){.wrapper{max-width:952px;max-width:59.5rem;-moz-background-origin:content;-o-background-origin:content-box;-webkit-background-origin:content;background-origin:content-box;-moz-background-clip:content;-o-background-clip:content-box;-webkit-background-clip:content;background-clip:content-box}}@media (min-width: 952px){.menu{width:74.47479%;float:right;margin-right:0}.banner .wrapper>div{width:48.94958%;float:left;margin-right:2.10084%}.services article{width:48.94958%;float:left;margin-right:2.10084%}.services article:nth-child(2n){float:right;margin-right:0}.equipe article{width:31.93277%;float:left;margin-right:2.10084%}.equipe article:nth-child(3n){float:right;margin-right:0}.servicos article{width:31.93277%;float:left;margin-right:2.10084%}.servicos article:nth-child(3n){float:right;margin-right:0}.two-columns article{width:48.94958%;float:left;margin-right:2.10084%}.two-columns article:nth-child(2n){float:right;margin-right:0}#artigos{width:48.94958%;float:left;margin-right:2.10084%}#depoimentos{width:40.44118%;float:right;margin-right:0}#contato{width:40.44118%;float:left;margin-right:2.10084%;padding-right:8.5084%}#formulario{width:57.45798%;float:right;margin-right:0}#blog .content{width:74.47479%;float:left;margin-right:2.10084%}#blog aside{width:23.42437%;float:right;margin-right:0}}*{-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box}body{font-family:"Open Sans", Arial sans-serif;font-size:14px;color:#424242;background:#FFFFFF;}strong{font-weight:700}a{text-decoration:none;color:#0f7505;-moz-transition:color 0.2s ease-out;-o-transition:color 0.2s ease-out;-webkit-transition:color 0.2s ease-out;transition:color 0.2s ease-out}a:hover{color:#224422;text-decoration:underline}p{line-height:140%}img{max-width:100%;height:auto}.btn{position:relative;display:inline-block;*display:inline;text-transform:uppercase;padding:8px 18px;margin-bottom:0;border:0;font-size:110%;letter-spacing:1px;font-family:"WebOswald";font-weight:400;line-height:20px;color:#FFFFFF;text-align:center;text-shadow:rgba(0,0,0,0.3) 1px 1px 1px;vertical-align:middle;cursor:pointer;background-color:#71ac54;-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:4px;*zoom:1;-moz-box-shadow:#416d2b 0 4px 0;-webkit-box-shadow:#416d2b 0 4px 0;box-shadow:#416d2b 0 4px 0}.btn:disabled{opacity:0.5}.btn:hover,.btn:focus{color:#FFFFFF;text-decoration:none;background-color:#669b4b;-moz-box-shadow:#365b24 0 4px 0;-webkit-box-shadow:#365b24 0 4px 0;box-shadow:#365b24 0 4px 0;-moz-transition:background-color 0.2s ease-out;-o-transition:background-color 0.2s ease-out;-webkit-transition:background-color 0.2s ease-out;transition:background-color 0.2s ease-out}.btn.active,.btn:active{outline:0;-moz-box-shadow:#416d2b 0 3px 0;-webkit-box-shadow:#416d2b 0 3px 0;box-shadow:#416d2b 0 3px 0;top:1px}.btn-large{padding:18px 20px;font-size:125%;letter-spacing:0}.center{text-align:center}.info{margin:12px 0 0;padding-bottom:5px;font-size:85%;color:#424242;border-bottom:1px solid #ebebeb}.info a{color:#424242}.info .fa-stack{width:1.3em}.socials{float:right}.socials i{font-size:120%}.clearfix{overflow:hidden;*zoom:1}.two-columns article{margin:40px 0}.form-msg{border:1px solid #FFFFFF;-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:4px;padding:10px;margin-bottom:30px !important}.form-msg i{margin-right:10px;font-size:250%;float:left}.form-error{border-color:#c29a9a;background:rgba(194,154,154,0.05)}.form-error i{color:#c29a9a}.form-success{border-color:#0f7505;background:rgba(15,117,5,0.05)}.form-success i{color:#0f7505}.fb-like-box,.fb-like-box span,.fb-like-box span iframe[style]{width:100% !important}.autocomplete-suggestions{border:1px solid #999;background:#FFF;cursor:default;overflow:auto;-moz-box-shadow:rgba(50,50,50,0.1) 2px 2px 2px;-webkit-box-shadow:rgba(50,50,50,0.1) 2px 2px 2px;box-shadow:rgba(50,50,50,0.1) 2px 2px 2px}.autocomplete-suggestion{padding:8px 5px;white-space:nowrap;overflow:hidden;cursor:pointer;color:#857f7f}.autocomplete-selected{background:#F0F0F0}.autocomplete-suggestions strong{font-weight:700;color:#336633}h2.title-page{font-family:"WebOswald";font-weight:400;font-size:240%;text-transform:uppercase;margin:40px 0 20px}h3.title-page{font-family:"WebOswald";font-weight:300;font-size:190%;margin:40px 0 30px}header{width:100%;border-bottom:3px solid #e1e1e1}h1{text-align:center}h1 a{display:block;margin-top:33px}.menu>ul{float:right;display:inline-table}.menu>ul>li{float:left;display:block;position:relative;margin:0;-moz-transition:background-color 0.2s ease-out;-o-transition:background-color 0.2s ease-out;-webkit-transition:background-color 0.2s ease-out;transition:background-color 0.2s ease-out}.menu>ul>li>a{font-family:"WebOswald";font-weight:400;display:table-cell;text-transform:uppercase;font-size:120%;color:#000000;vertical-align:middle;padding:0 34px;height:120px}.menu>ul>li>a:hover{text-decoration:none}.menu>ul>li:hover,.menu>ul>li.ativo{background-color:#f4f4f4}.menu>ul>li:before{content:"";height:0px;position:absolute;top:-4px;left:0;width:100%;-moz-transition:background-color 0.2s ease-out, height 0.2s ease-out;-o-transition:background-color 0.2s ease-out, height 0.2s ease-out;-webkit-transition:background-color 0.2s ease-out, height 0.2s ease-out;transition:background-color 0.2s ease-out, height 0.2s ease-out}.menu>ul>li:hover:before,.menu>ul>li.ativo:before{height:4px;background-color:#8fa367}.menu>ul>li:hover>ul{display:block}.menu>ul ul{display:none;position:absolute;top:100%;z-index:9999}.menu>ul ul li{float:none;position:relative;border-bottom:1px solid #FFFFFF}.menu>ul ul li a{font-family:"WebOswald";font-weight:400;font-size:100%;text-transform:uppercase;color:#000000;background-color:#f4f4f4;display:block;padding:15px 20px;white-space:nowrap;-moz-transition:background-color 0.2s ease-out;-o-transition:background-color 0.2s ease-out;-webkit-transition:background-color 0.2s ease-out;transition:background-color 0.2s ease-out}.menu>ul ul li a:hover{text-decoration:none;background-color:#ececec};color:#000000;padding:35px 0;height:300px;font-family:"WebOswald"}.banner h2{display:inline-block;vertical-align:middle;*vertical-align:auto;*zoom:1;*display:inline;text-transform:uppercase;color:#000000;padding:10px;margin:0 0 10px 0;font-weight:700;font-size:230%;letter-spacing:0}.banner h3{font-weight:300;font-size:170%;padding:5px 10px;margin-bottom:15px}.banner h3 strong{font-weight:400;color:#000000}.banner .btn{margin-left:45px;margin-top:20px}.services{padding:40px 0;text-align:center}.services .circle{width:17%;height:76px;line-height:76px;display:inline-block;border:4px solid #c2c2c2;-moz-border-radius:50%;-webkit-border-radius:50%;border-radius:50%;text-align:center;position:relative;float:left;font-size:140%}.services .circle i{color:#8ba555; margin-top:13px;}.services .text{text-align:left;width:78%;float:right}.services h2{font-size:170%;font-weight:700}.services h3{font-size:130%;font-weight:400}.services p{margin-top:15px}.services .btn{margin-top:20px}.banner-loja .wrapper{border-top:1px solid #F0F0F0;padding-top:40px;margin-top:40px}.equipe p{margin:20px 0}.equipe article{margin-bottom:10px;padding:20px;text-align:center;-moz-transition:background-color 0.2s ease-out;-o-transition:background-color 0.2s ease-out;-webkit-transition:background-color 0.2s ease-out;transition:background-color 0.2s ease-out}.equipe article:hover{background-color:rgba(0,0,0,0.1);-moz-border-radius:6px;-webkit-border-radius:6px;border-radius:6px}.equipe article img{-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:4px;-moz-box-shadow:#CCCCCC 0 0 10px;-webkit-box-shadow:#CCCCCC 0 0 10px;box-shadow:#CCCCCC 0 0 10px;-moz-transition:box-shadow 0.2s ease-out;-o-transition:box-shadow 0.2s ease-out;-webkit-transition:box-shadow 0.2s ease-out;transition:box-shadow 0.2s ease-out}.equipe article h3{color:#336633;font-weight:600;font-size:120%;margin:20px 0}.servicos article{text-align:center;margin-bottom:10px;border:1px solid #f0f0f0;-moz-border-radius:8px;-webkit-border-radius:8px;border-radius:8px}.servicos article .circle{width:100px;height:100px;line-height:116px;display:inline-block;border:4px solid #c2c2c2;-moz-border-radius:50%;-webkit-border-radius:50%;border-radius:50%;text-align:center;position:relative}.servicos article .circle i{color:#8ba555;margin-top:13px;}.servicos article h2{font-size:160%;font-family:"WebOswald";font-weight:400;text-transform:uppercase;margin:20px 0 10px}.servicos article h3{font-size:120%;font-weight:600;margin-bottom:10px}.servicos article a{padding:25px 15px;display:block;color:#000000;-moz-transition:background-color 0.2s ease-out;-o-transition:background-color 0.2s ease-out;-webkit-transition:background-color 0.2s ease-out;transition:background-color 0.2s ease-out}.servicos article a:hover{text-decoration:none;background-color:#f4f4f4}.interna p{line-height:160%;margin:15px 0}.interna a{font-weight:700}.interna .fa-stack{margin:0 10px 30px 0}.interna .preview{float:left;margin:0 15px 0 0}#artigos-depoimentos{background-color:#FFFFFF}#artigos-depoimentos h2{text-align:left;background-position:bottom left}#artigos article{padding:0;border:none}#artigos article h3{font-size:115%;margin-bottom:20px}#artigos article p{margin-bottom:10px}#depoimentos article{margin-bottom:35px;overflow:hidden;*zoom:1}#depoimentos img{-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:4px;float:left;position:relative;top:3px}#depoimentos p{width:83%;float:right;font-size:90%}#depoimentos p.nome{font-weight:600}#depoimentos p.nome span{color:#0f7505}#fale p{margin-bottom:50px}#contato{border-right:1px dashed #d9d9d9}#contato a{color:#000000;display:block;width:100%;padding:5px 0;margin-bottom:35px;-moz-transition:padding 0.2s ease-out;-o-transition:padding 0.2s ease-out;-webkit-transition:padding 0.2s ease-out;transition:padding 0.2s ease-out;overflow:hidden;*zoom:1}#contato a.tel{margin-top:20px}#contato a:hover{padding-left:5px;text-decoration:none}#contato a:hover span.icon{background-color:#000000}#contato a:hover i{color:#FFFFFF}#contato a span.icon{position:relative;top:-3px;text-align:center;background-color:#FFFFFF;-moz-border-radius:50%;-webkit-border-radius:50%;border-radius:50%;width:26px;height:26px;display:inline-block;font-size:120%;-moz-transition:background-color 0.2s ease-out;-o-transition:background-color 0.2s ease-out;-webkit-transition:background-color 0.2s ease-out;transition:background-color 0.2s ease-out;margin-right:6px;-moz-box-shadow:#CCCCCC 1px 1px 1px;-webkit-box-shadow:#CCCCCC 1px 1px 1px;box-shadow:#CCCCCC 1px 1px 1px}#contato a span.icon i{position:relative;top:5px;-moz-transition:color 0.2s ease-out;-o-transition:color 0.2s ease-out;-webkit-transition:color 0.2s ease-out;transition:color 0.2s ease-out}#formulario>p{font-weight:600;font-size:80%;text-align:right;margin-bottom:10px}#formulario label,#formulario .label{display:block;margin-bottom:10px;font-weight:600}#formulario input[type="text"],#formulario textarea{font-family:"Open Sans", Arial sans-serif;width:100%;-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:4px;border:1px solid #e6e6e6;background-color:#FFFFFF;padding:10px;font-size:100%;margin-top:5px}#formulario .tel{width:47%;margin-right:6%;float:left}#formulario .cidade{width:47%;overflow:hidden;*zoom:1}#formulario textarea{height:100px}#formulario input[type="submit"]{float:right}#formulario ul{margin:15px 0;position:relative;overflow:hidden;*zoom:1}#formulario ul li{width:50%;display:inline-block;float:left;padding:5px}#formulario ul label{width:100%;background-color:rgba(255,0,0,0.5);padding:8px;-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:4px;background-color:rgba(0,0,0,0.03);border:1px solid rgba(0,0,0,0.06)}#formulario ul label,#formulario ul input[type="checkbox"]+span,#formulario ul input[type="checkbox"]+span::before{display:inline-block;vertical-align:middle;cursor:pointer}#formulario ul input[type="checkbox"]{opacity:0;position:absolute}#formulario ul input[type="checkbox"]+span{color:#424242;line-height:140%}#formulario ul label:hover span::before{-moz-box-shadow:rgba(0,0,0,0.3) 0 0 2px;-webkit-box-shadow:rgba(0,0,0,0.3) 0 0 2px;box-shadow:rgba(0,0,0,0.3) 0 0 2px}#formulario ul label:hover span{color:#000000}#formulario ul input[type="checkbox"]+span::before{content:"";width:12px;height:12px;margin:-3px 4px 0 0;border:solid 1px #a8a8a8;line-height:14px;text-align:center;-moz-border-radius:2px;-webkit-border-radius:2px;border-radius:2px;background:#f6f6f6;background-image:url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PHJhZGlhbEdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgY3g9IjUwJSIgY3k9IjUwJSIgcj0iMTAwJSI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2Y2ZjZmNiIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iI2RmZGZkZiIvPjwvcmFkaWFsR3JhZGllbnQ+PC9kZWZzPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==');background-size:100%;background-image:-moz-radial-gradient(#f6f6f6, #dfdfdf);background-image:-webkit-radial-gradient(#f6f6f6, #dfdfdf);background-image:radial-gradient(#f6f6f6,#dfdfdf)}#formulario ul input[type="checkbox"]:checked+span::before{color:#666}#formulario ul input[type="checkbox"]:checked+span::before{content:"✔";font-size:12px}footer{padding-top:20px}#copyright{color:#FFFFFF;background-color:#444444;padding:20px 0;text-align:right;font-size:90%}article.post{margin-bottom:60px;border-bottom:1px solid #e9e7e7;padding-bottom:40px;overflow:hidden;*zoom:1}article.post time{text-align:center;display:block;float:left;width:10%;background-color:#f5f5f5;color:#857f7f;font-weight:600;font-size:160%;line-height:200%;margin-right:4%}article.post time span{display:block;background-color:#857f7f;color:#FFFFFF;text-transform:uppercase;font-size:60%;line-height:100%;padding:5px 10px}article.post h3{text-transform:inherit;font-weight:700;font-size:180%;margin-top:0 !important}article.post h3 a{color:#000000}article.post p{float:right;width:86%;font-size:90%;margin-bottom:40px}article.post p+a{float:right;font-size:90%}#blog{padding:40px 0}#blog .content{padding-right:30px}#blog .content h2{letter-spacing:-1px;text-align:inherit;text-transform:inherit;background-position:left bottom;font-size:220%;font-weight:bold;margin-bottom:40px}#blog .content .info{font-size:90%}#blog .content .info p{margin:0}#blog .content .info p span,#blog .content .info p time{font-weight:700}#blog .content h3{text-transform:inherit;font-size:180%;font-weight:700;margin-top:40px}#blog .content p{margin-top:20px}#blog .content ul{margin:20px}#blog .content ul li{line-height:160%}#blog .content ul li::before{font-family:FontAwesome;content:"";font-size:65%;padding-right:10px}#blog .content blockquote{margin:10px;overflow:hidden;*zoom:1}#blog .content blockquote::before{font-family:"FontAwesome";content:"";font-size:2.5em;color:#CCCCCC;margin-top:25px;float:left;width:5%}#blog .content blockquote p{font-style:italic;float:right;width:90%}#blog aside div{margin-bottom:20px;font-size:90%}#blog aside h3{font-weight:600;text-transform:inherit;font-size:110%;margin-bottom:15px}#blog aside li{padding:10px 0}#blog aside li a:hover{text-decoration:none}#blog aside li a:before{font-family:FontAwesome;content:"";font-size:60%;padding-right:10px}@media print{*{background:transparent !important;color:#000 !important;box-shadow:none !important;text-shadow:none !important}a,a:visited{text-decoration:underline}a[href]:after{content:" (" attr(href) ")"}abbr[title]:after{content:" (" attr(title) ")"}.ir a:after,a[href^="javascript:"]:after,a[href^="#"]:after{content:""}pre,blockquote{border:1px solid #999;page-break-inside:avoid}thead{display:table-header-group}tr,img{page-break-inside:avoid}img{max-width:100% !important}@page{margin:0.5cm}p,h2,h3{orphans:3;widows:3}h2,h3{page-break-after:avoid}}
body {background: #EE1D23;}
	.centered {
  position: relative;
  left: 50%;
  /* bring your own prefixes */
  transform: translate(-50%, 15px);
}
#post-38 {width:100% !important; padding-right:0 !important;margin-right:0 !important;}
.cover .logo h1 a {margin-top:0;}
.chamada {
	width:680px;
	position:relative;
	font-family:"Open Sans";
	font-size:25px;
	color:#fff;
	font-style:italic;
	letter-spacing:-0.3px;
}
.chamada p {font-family:"Open Sans";}
.chamada span {font-weight:bold;}
.chamada .logo {width:432px;margin:0 auto;}
.chamada .logo h1 {width:100%;}
.desc {width:100%;float:left;margin:30px 0 0 0}
.desc p {text-align:justify;line-height:29px;font-size:15px;}
.email {text-align:center; width:100%;float:left;}
.email .loja {margin-top:20px;}
.email img {margin-top:20px;}
.social {width:210px;height:85px;position:relative;margin:0 auto;}
.social a.facebook, .social a.youtube, .social a.instagram {margin-bottom:15px;width:58px;height:58px;float:left;background-size:59px;}
.social a.facebook {margin-top:45px;margin-right:18px;background-image:url(http://192.185.214.38/~abracei/facebook.png);}
.social a.youtube {margin-top:45px;margin-right:18px;background-image:url(http://192.185.214.38/~abracei/youtube.png);}
.social a.instagram {margin-top:45px;background-image:url(http://192.185.214.38/~abracei/instagram.png);}

#mc_embed_signup{clear:left; }
#mc_embed_signup form {padding:0 !important;}
#mc_embed_signup .mc-field-group input {background-color:transparent;}
#mc_embed_signup input {border: 1px solid #fff !important;}
#mc_embed_signup input.email {background-color:#fff;}
#mc_embed_signup input.button {background-color:#fff;color:#00C585;margin:0 !important;}
#mc_embed_signup .button {border-radius:0px !important;}
#mc_embed_signup input.email {margin:0 0 0 0 !important;}
::-webkit-input-placeholder {color: #EE1D23;}
:-moz-placeholder {color:#EE1D23;}
::-moz-placeholder {color:#EE1D23;}
:-ms-input-placeholder {color:#EE1D23;}
input[type=email] {color:##EE1D23 !important;}
input[type=submit] {background-color:#C01216 !important;color:#fff !important;}

#mc_embed_signup input.email, #mc_embed_signup input.button {font-weight:300 !important;font-family:"Open Sans","Helvetica Neue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif !important;display:inline-block !important;font-size:14px;}
#mc_embed_signup input.button {width:40% !important;height:55px;font-weight:normal !important;}
#mc_embed_signup input.email {padding:8px 15px !important;width:60% !important;height:55px;font-style:italic;}
.email {text-align:left !important;}

.campos {border:1px solid #fff;}
@media (max-width:700px) {
.chamada {width:90%;}
.chamada .logo {width:100%;}
}
@media (max-width:480px) {
	#mc_embed_signup input.email {width:90% !important;margin-left:5% !important;}
	#mc_embed_signup input.button {min-width:250px;width:90% !important;margin-left:5% !important;}
	.centered {
  top: 15px;
  left: 5%;
  /* bring your own prefixes */
  transform: none;
}
.campos {border:none;}
	
}

/* Continua */

	</style>
</head>
<div class="cover">
	<div class="chamada centered">
		<div class="logo">
			<h1><a title="Abracei" href="http://abracei.com.br/"><img class="logoimg" width="592" height="246" alt="Abracei" src="http://192.185.214.38/~abracei/logo-manutencao.png" /></a></h1>
		</div>
		<div class="desc">
			<h1 style="margin-bottom:30px;text-align:center;font-style:italic;color:#fff;">Estamos em renovação para melhor atendê-los!</h1>
			<p>Estamos melhorando tudo para deixar sua experiência de compra cada vez melhor!</p>
			<p>A ABRACEI 2016 vem aí! Mais rápida, adaptada para mobile e com grandes lançamentos autorais!</p>
			<p style="margin-bottom:20px;">Estaremos off-line por algumas semanas para uma grande reformulação!</p>
			<p style="margin-bottom:20px;"><img src="http://192.185.214.38/~abracei/produtos.jpg" width="680" height="340" /></p>
			<div class="lista">
				<!-- Begin MailChimp Signup Form -->
				<link href="//cdn-images.mailchimp.com/embedcode/slim-081711.css" rel="stylesheet" type="text/css">
				<div id="mc_embed_signup">
				<form action="//abracei.us9.list-manage.com/subscribe/post?u=3cfc60decc8b4efb392cceae8&amp;id=67ad02a4b5" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					<div id="mc_embed_signup_scroll">
					<p style="font-style:italic;text-align:center;">Cadastre seu email e concorra a um produto:</p>
					<div class="campos">
					<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Digite seu email..." required><input type="submit" value="Me avise quando a Abracei voltar" name="subscribe" id="mc-embedded-subscribe" class="button">
					</div>
					<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_3cfc60decc8b4efb392cceae8_67ad02a4b5" tabindex="-1" value=""></div>
					<div class="clear"></div>
					</div>
				</form>
				</div>
				<!--End mc_embed_signup-->
			</div>
		</div>
		<div class="social">
			<a class="facebook" href="https://www.facebook.com/abracei" target="_blank"></a>
			<a class="youtube" href="https://www.youtube.com/channel/UCV9k9eZ4pCajzVimQLFkJZA" target="_blank"></a>
			<a class="instagram" href="https://www.instagram.com/abracei/" target="_blank"></a>
		</div>
	</div>
</div>