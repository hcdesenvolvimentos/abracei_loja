
<?php 

global $woocommerce;
global $product;
global $configuracao;
global $current_user;
$urlMinhaConta 	= get_permalink(get_option('woocommerce_myaccount_page_id'));
$urlCarrinho    = WC()->cart->get_cart_url();
$urlCheckout 	= WC()->cart->get_checkout_url();
$urlLogout 	= wp_logout_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );

 ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php wp_title(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" type="image/png" href="//abracei.com.br/wp-content/themes/abraco/images/favicon.png">
	<link rel="alternate" hreflang="pt-br" href="//abracei.com.br/" />
    <!-- Le styles -->
    <link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php wp_enqueue_script("jquery"); ?>
    <?php wp_head(); ?>
	<link href="<?php echo get_template_directory_uri(); ?>/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<script type="text/javascript" src="//abracei.com.br/wp-content/themes/abraco/js/classie.js"></script>
	<script type="text/javascript" src="//abracei.com.br/wp-content/themes/abraco/js/smooth.js"></script>
	<script type="text/javascript" src="//abracei.com.br/wp-content/themes/abraco/js/mobile-menu.js"></script>
	<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '1562287477355561');
		fbq('track', "PageView");</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=1562287477355561&ev=PageView&noscript=1"
		/></noscript>
	<!-- End Facebook Pixel Code -->
	<link rel='stylesheet' id='fontawesome-css'  href='https:////maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css?ver=30a184689e18419aecee5490d45dfde9' type='text/css' media='all' />
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-53882238-1"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-53882238-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-53882238-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-53882238-1');
</script>
  </head>
  <body <?php body_class(); ?>>
<div id="wrap">
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5&appId=193484447521777";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<div class="container-fluid">
	<header>
		<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">

			<div class="row" class="areaLogoMenu">
		  		
		  		<div class="col-sm-3">
		  			<a class="brand" href="<?php echo site_url(); ?>">
		  				<img src="//abracei.com.br/wp-content/uploads/2016/04/logo.png" width="300" height="100" title="Abracei Presentes Criativos" alt="Logomarca Abracei" />
	  				</a>
		  		</div>
		  		<div class="col-sm-6">
		  			<div class="barradePesquisa">
		  				<?php echo do_shortcode('[wcas-search-form]'); ?>	
		  			</div>
		  		</div>
		  		<div class="col-sm-3">
		  			<div class="menuConta">
		  				<?php if (is_user_logged_in() == true): ?>
		  					Olá <strong><a href="<?php echo get_home_url() ?>/minha-conta/ "><strong><?php echo $current_user->user_login; ?></strong></a> | </strong>	<a href="<?php echo $urlLogout ?>">Sair</a>
		  				<?php else: ?>
		  				<span class="minhaContaLin"><i class="fas fa-user"></i> Minha conta</span> | <a href="<?php echo get_home_url() ?>/minha-conta/ ">Cadastre-se</a>
		  				<?php endif; ?>
		  			</div>
		  		</div>
		  	</div>
		 
		  <div class="container">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			</a>
			
			
			<div class="nav-collapse collapse">
			  <ul class="nav">


			  </ul>

			</div><!--/.nav-collapse -->
		  </div>
		</div>
		</div>
		<div class="menu-container">
			<div class="container" style="width:100%;max-width: 1444px;">
				<?php
					wp_nav_menu(array(
						'menu' => 'Main Navigation',
						'container_id' => 'cssmenu',
					));
				?>
			</div>
		</div>

	</header>

	<!-- FORMULÁRIOS MODAL -->
<div class="formulariosModal">
	<!-- FORMULÁRIO DE LOGIN -->
	<div class="produtoGerais login">
		<button class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></button>
		<p>Entrar com sua conta Abracei</p>
		
		<!-- LINK PARA SE CADASTRAR -->
		<?php wp_nonce_field( 'woocommerce-register' ); ?>
		<?php echo '<span class="subTitulo">	Não possui conta em nossa loja? <a class="cadastrar" href="' .get_permalink(woocommerce_get_page_id('myaccount')). '?action=register">Registre-se</a></span>'; ?>
		<?php do_action( 'woocommerce_register_form_end' ); ?>

		
		
		

		<!-- FORMULÁRIO DE LOGIN  -->
		<form method="post" class="">

			<?php do_action( 'woocommerce_login_form_start' ); ?>
			<input type="text" placeholder="E-mail" class="inputText"  name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>">
			<input type="password" placeholder="Senha" class="inputSenha" name="password" id="password">
			<?php do_action( 'woocommerce_login_form' ); ?>


			<?php wp_nonce_field( 'woocommerce-login' ); ?>
			<!-- LINK PARA REDEFINIR A SENHA -->
			<a  href="<?php //echo esc_url( wp_lostpassword_url() ); ?>" class="linkRecuperarSenha" id="modalLoginRecuperarSenha">Esqueci minha senha</a>

			<input type="submit" value="Entrar" class="btnEnviar"  name="login">

			<?php do_action( 'woocommerce_login_form_end' ); ?>

		</form>
	</div>

</div>

<!-- MODAL POPUP -->
<div class="conteudoModal" id="conteudoModal" >
	

	<div class="modalCadastrese">
		<button class="fecharPopup">
			<i class="fas fa-times" aria-hidden="true"></i>
       </button>
		

		<img src="<?php bloginfo('template_directory'); ?>/abracei.png" alt="">
		<div class="formModal">
			<!--START Scripts : this is the script part you can add to the header of your theme-->
				<script type="text/javascript" src="https://abracei.com.br/wp-includes/js/jquery/jquery.js?ver=2.8.2"></script>
				<script type="text/javascript" src="https://abracei.com.br/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.8.2"></script>
				<script type="text/javascript" src="https://abracei.com.br/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.8.2"></script>
				<script type="text/javascript" src="https://abracei.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.8.2"></script>
				<script type="text/javascript">

		        /* <![CDATA[ */
		            var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"https://abracei.com.br/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
		                /* ]]> */
		            </script><script type="text/javascript" src="https://abracei.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.8.2"></script>
			<!--END Scripts-->

			<div class="widget_wysija_cont html_wysija">
				<div id="msg-form-wysija-html5adf3e37df949-2" class="wysija-msg ajax">		
				</div>

				<form id="form-wysija-html5adf3e37df949-2" method="post" action="#wysija" class="widget_wysija html_wysija">

					<input type="text" name="wysija[user][email]" class=" inputText validate[required,custom[email]]" title="E-mail" placeholder="E-mail" value="" />		

					<!-- <input type="text" name="wysija[user][abs][email]" class=" inputText validated[abs][email]" value="" />						  -->
					<input class="btnEnviar wysija-submit wysija-submit-field" type="submit" value="Enviar" />

					<input type="hidden" name="form_id" value="2" />
				    <input type="hidden" name="action" value="save" />
				    <input type="hidden" name="controller" value="subscribers" />
				    <input type="hidden" value="1" name="wysija-page" />
    
   					<input type="hidden" name="wysija[user_list][list_ids]" value="3" />
 
				</form>
			</div>
		</div>
	</div>
</div>

<style>

.conteudoModal{
	background: rgba(0, 0, 0, 0.61);
	position: fixed;
	top: 0;
	width: 100%;
	height: 100vh;
	z-index: 9999;
	left: 0;
	display: none; 
}
	.conteudoModal .modalCadastrese {
		max-width: 500px;
 		position: absolute;
 		top: 50%;
 		left: 50%;
 		transform: translate(-50%, -50%);
 		background: #fff;
 		border-radius: 5px;
 		padding: 6px 20px;
 		min-height: 250px;
	
	}
	.conteudoModal .modalCadastrese button {
		display: block;
	    width: 100%;
	    max-width: 24px;
	    min-height: 22px;
	    margin-bottom: 4px;
        font-size: 21px;
	    font-weight: 700;
	    line-height: 1;
	    color: #000;
	    opacity: .2;
	    background: none;
	    border: none;
	    float: right;
	}

	.conteudoModal .modalCadastrese .fecharPopup{
		display: block;
	    width: 100%;
	    max-width: 24px;
	    min-height: 22px;
	    margin-bottom: 4px;
        font-size: 21px;
	    font-weight: 700;
	    line-height: 1;
	    color: #000;
	    opacity: .2;
	    background: none;
	    border: none;
	    float: right;
	}

	.conteudoModal .modalCadastrese  button:hover {
  
	    color: #333;  
	    opacity: 1.0;

	}   
	

	.conteudoModal .modalCadastrese img{
		width: 100%;
	    max-width: 470px;
	    height: auto;
	}


	.conteudoModal .modalCadastrese .formModal{
		min-height: 60px; 

    }
	.conteudoModal .modalCadastrese .formModal input[type="text"]{
		display: inline-block;
 		width: 100%;
        max-width: 310px;
 		margin-top: 10px;
        min-height: 40px;
        padding: 3px 10px;
        border: 1px solid #a9a9a9;

	}

	 .formModal input[type="submit"]{
		width: 100%;
        max-width: 145px;
		display: inline-block;
		margin-top: 14px;
		min-height: 40px;
		background: #33aac8;
		color: #fff;
		text-transform: uppercase;
		border: none;
		border-radius: 5px;
		font-weight: 700;
 	}
 	
 	.formModal form .formErrorContent{
		color: white;
		display: block;
		text-align: left;
		font-size: 12px;
		font-weight: 700;

 	}
	.form-validation-field-0formError.parentFormform-wysija-html5adf3e37df949-2.formError{
		opacity: 0.87;
		top: 69px!important;
		left: 0!important;
		width: 100%;
		color: #fff;
		padding: 10px;
	
	}
	.html_wysija{
		position: relative;
	}
	
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

 <style>
 #cssmenu>ul>li>a{
 	font-size: 15px;
 }
 	.formulariosModal{
 		background: #000000d6;
 		position: fixed;
 		top: 0;
 		width: 100%;
 		height: 100vh;
 		z-index: 9999;
 		left: 0;
 		display: none;
 	}
 	.produtoGerais{
 		max-width: 500px;
 		margin: 0 auto;
 		position: absolute;
 		top: 50%;
 		left: 50%;
 		transform: translate(-50%, -50%);
 		background: #fff;
 		border-radius: 5px;
 		padding: 20px;
 		min-height: 250px;
 	}
 	.produtoGerais button{

 	}
 	.produtoGerais p{
 		font-size: 20px;
 		text-align: center;
 	}
 	.produtoGerais .subTitulo{
 		display: block;
 		text-align: center;
 	}
 	.produtoGerais input[type="text"]{
 		display: block;
 		width: 100%;
 		max-width: 90%;
 		margin: 0 auto;
 		margin-top: 10px;
 		outline: none;
 	}
 	.produtoGerais input[type="password"]{
 		display: block;
 		width: 100%;
 		max-width: 90%;
 		margin: 0 auto;
 		margin-top: 10px;
 		outline: none;
 	}
 	.produtoGerais input[type="submit"]{
		display: block;
		margin: 0 auto;
		width: 90%;
		margin-top: 14px;
		height: 40px;
		background: #33aac8;
		color: #fff;
		text-transform: uppercase;
		border: none;
		border-radius: 5px;
		font-weight: 700;
		outline: none;
 	}
 	.linkRecuperarSenha{
 		font-size: 13px;
 		display: block;
 		max-width: 90%;
 		margin: 4px auto;
 		color: #747270;
 	}
	 .areaLogoMenu{
	 	width: 100%;
	 	max-height: 1400px;
	 }
 	.woocommerce img, .woocommerce-page img{
 		height: auto;
 		max-width: 100%;
 		display: block;
 		margin: 0 auto;
 		
 	}
 	.menu-container{
		top: 120px;
 	}
 	header.menor .menu-container{
 		    top:95px;
 	}
 	.barradePesquisa .dgwt-wcas-no-submit .dgwt-wcas-ico-loupe{
 		height: initial;
 		left: initial;
 		right: 10px;
 		opacity: 1;
 		fill: #000;
 		max-width: 22px;
 	}
 	.barradePesquisa input[type="search"]{
		border: none!important;
		padding: 10px!important;
 	}
 	.barradePesquisa{
 		display: block;
 		max-width: 450px;
 		margin: 0 auto;
		padding-top: 30px;

 	}
 	.menuConta{
		color: #fff;
		padding-top: 45px;
 	}
 		.menuConta a{
			color: #fff;
			font-size: 17px;
			font-weight: 500;
 		}
 		.menuConta span{
			color: #fff;
			font-size: 17px;
			font-weight: 500;
			cursor: pointer;
 		}
			.menuConta a i{
 		
 			}
	/* CSS CHECKAUT */
	#prazo-diferenciado{
		display: none;
	}
	.woocommerce-account .woocommerce-Input{
		border-radius: 5px;
		height: 44px!important;
		display: block;
	}
	.woocommerce-account .woocommerce-form__label{
		display: block;
		line-height: 15px;
		text-align: center;
		font-size: 15px;
	}
	.woocommerce-account .woocommerce form .form-row .input-checkbox{
			display: inline-block;
			margin: -2px 8px 0 0;
			text-align: center;
			vertical-align: middle;
			margin: 8px 0 0 -17px;
	}
	.woocommerce-MyAccount-navigation ul{
		padding: 0;
		list-style: none;
		border: solid 1px #ccc;
		border-top: none;
		border-bottom: none;
		border-left: none;
		margin: 0;
	}
		.woocommerce-MyAccount-navigation ul li{
			padding: 10px 0;
		}
			.woocommerce-MyAccount-navigation ul li:hover{
				padding: 10px 0;
				background: #535353;
			}
			.woocommerce-MyAccount-navigation ul li a{
				text-decoration: none;
    			color: #535353;
    			padding: 0 10px;
    			display: block;
    			width: 100%;
			}
			.woocommerce-MyAccount-navigation ul li:hover a{
    			color: #fff;
			}
			
			.woocommerce-MyAccount-content > p{
				text-align: center;
				padding: 12px;
			}
				.woocommerce-MyAccount-content > p a{
					font-size: 17px;
					padding: 0 10px;
					text-decoration: underline;
				}
			.woocommerce-MyAccount-content{
				float: inherit;
				width: 100%;
				padding: 0 30px;

			}
	.span.amount{
		font-size: 25px;
	}
	.woocommerce #content table.cart td.actions .input-text, .woocommerce table.cart td.actions .input-text, .woocommerce-page #content table.cart td.actions .input-text, .woocommerce-page table.cart td.actions .input-text{
		    width: 152px;
	}
	.logged-in.woocommerce-checkout .input-text {
		border-radius: 5px!important;
		height: 40px;
	}

	.woocommerce-billing-fields .form-row:hover  .input-text {
		border-radius: 0 5px 5px 5px!important;
		height: 40px;
		transition:All .1s ease;
		-webkit-transition:All .1s ease;
		-moz-transition:All .1s ease;
		-o-transition:All .1s ease;
	}
	.shipping_address .form-row:hover  .input-text {
		border-radius: 0 5px 5px 5px!important;
		height: 40px;
		transition:All .1s ease;
		-webkit-transition:All .1s ease;
		-moz-transition:All .1s ease;
		-o-transition:All .1s ease;
	}
	.logged-in.woocommerce-checkout .select2-container--default .select2-selection--single{
		border-radius: 5px!important;
		height: 40px;
		transition:All .1s ease;
		-webkit-transition:All .1s ease;
		-moz-transition:All .1s ease;
		-o-transition:All .1s ease;
	}
	span.amount{
		color: #535353;
    font-size: 20px;
    line-height: 40px;
    font-weight: bold;
    margin-bottom: 0;
	}
	.woocommerce-billing-fields h3{
		display: block;
		background: #999999;
		text-align: center;
		color: #fff;
		padding: 35px 0;
		border-radius: 5px;
	}
	#ship-to-different-address{
		display: block;
		background: #999999;
		text-align: center;
		color: #fff;
		padding: 25px 0;
		border-radius: 5px;
		width: 100%;
	}
	.woocommerce-additional-fields textarea{
		-webkit-box-sizing: border-box;
		box-sizing: border-box;
		width: 100%;
		min-height: 88px;
		/* text-align: center; */
		padding: 20px;
	}
	.woocommerce form .form-row .input-checkbox{
		margin: 9px -20px;
	}
   	.woocommerce form .form-row{
   		position: relative;
   		padding-top: 20px;
		margin-top: 20px!important;
   	}	
	   	.woocommerce-billing-fields .form-row label{
			font-weight: 200;
			font-size: 12px;
			text-transform: uppercase;
			left: 3px;
			top: -10px;
			position: absolute;
			transition:All .1s ease;
			-webkit-transition:All .1s ease;
			-moz-transition:All .1s ease;
			-o-transition:All .1s ease;
	   	}
   		.woocommerce-billing-fields .form-row:hover label{
			opacity: 1;
			
			background: #535353;
			border-radius: 5px 5px 0px 0px;
			color: #fff;
			padding: 3px 10px;
			transition:All .1s ease;
			-webkit-transition:All .1s ease;
			-moz-transition:All .1s ease;
			-o-transition:All .1s ease;
   		}
   		.shipping_address .form-row label{
			font-weight: 200;
			font-size: 12px;
			text-transform: uppercase;
			left: 3px;
			top: -10px;
			position: absolute;
			transition:All .1s ease;
			-webkit-transition:All .1s ease;
			-moz-transition:All .1s ease;
			-o-transition:All .1s ease;
	   	}
   		.shipping_address .form-row:hover label{
			opacity: 1;
			
			background: #535353;
			border-radius: 5px 5px 0px 0px;
			color: #fff;
			padding: 3px 10px;
			transition:All .1s ease;
			-webkit-transition:All .1s ease;
			-moz-transition:All .1s ease;
			-o-transition:All .1s ease;
   		}
   		.shipping_address:hover .form-row .woocommerce-form__label.woocommerce-form__label-for-checkbox.checkbox{
   			top: inherit!important;
   			font-weight: inherit!important;
   			font-size: inherit!important;
   			text-transform: inherit!important;
   			left: inherit!important;
   			opacity: 1!important;
   			position: inherit!important;
   		}
   		.woocommerce form .form-row input.input-text, .woocommerce form .form-row textarea{
   			border-radius: 5px;
   		}
 </style>


 <script type="text/javascript">



	function writeCookie(name,value,days) {
		var date, expires;
		if (days) {
			date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			expires = "; expires=" + date.toGMTString();
		}else{
			expires = "";
		}
		document.cookie = name + "=" + value + expires + "; path=/";
	}

	function readCookie(name) {
		var i, c, ca, nameEQ = name + "=";
		ca = document.cookie.split(';');
		for(i=0;i < ca.length;i++) {
			c = ca[i];
			while (c.charAt(0)==' ') {
				c = c.substring(1,c.length);
			}
			if (c.indexOf(nameEQ) == 0) {
				return c.substring(nameEQ.length,c.length);
			}
		}
		return '';
	}


	$('#conteudoModal').on('hide.bs.modal', function (e) {
		var checkModal = false;
		writeCookie('modal', checkModal, 1);
	});
	if (!readCookie('modal')) {

			// It is true if mouse is near the top
			var mouse_position_switch = false;
			var modalshow = true;

			// Event listener to check if mouse leave the page body
			$(document.body).on('mouseleave', function(e) {
			   // Check if mouse is near the top
			   if(mouse_position_switch){

			       // Show modal
			       if (modalshow == true) {
			       	$("#conteudoModal").show();
			       	$("#conteudoModal").css({'display':'block'});
			       	modalshow = false;
			       }


			   }
			});

			// Event listener to set if mouse is near the top or not
			$(document.body).on('mousemove', function(e) {
				if(e.clientY < 100){
					mouse_position_switch = true;
				}else{
					mouse_position_switch = false;
				}

			});


		};
	

 //----- FECHAR POPUP ---->		
	$( ".fecharPopup" ).click(function() {
 		$(".conteudoModal").fadeOut();
	});


 //----- FECHAR FORMULARIO DE LOGIN ---->

		$( ".minhaContaLin" ).click(function() {
 		$(".formulariosModal").fadeIn();
	});
	$( ".close" ).click(function() {
 		$(".formulariosModal").fadeOut();
	});

</script>
