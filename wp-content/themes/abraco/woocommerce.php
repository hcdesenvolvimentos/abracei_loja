<?php get_header(); ?>

<div class="row">
	<div class="container">
		<div class="span12">
			<?php if ( is_singular( 'product' ) ) {
     woocommerce_content();
  }else{
   //For ANY product archive.
   //Product taxonomy, product search or /shop landing
    woocommerce_get_template( 'archive-product.php' );
  } ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>