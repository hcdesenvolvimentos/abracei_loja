<?php
/**
 * Template Name: Home
 */
get_header();
?>

	<div class="row">
	  <div class="span12">
		<div class="container">
			<div class="slider"><?php echo do_shortcode("[metaslider id=337]"); ?></div>
			<div class="lista-produtos">
				<?php echo do_shortcode("[recent_products per_page=9 columns=3]"); ?>
			</div>
		</div>
	  </div>
	</div>

<?php get_footer(); ?>