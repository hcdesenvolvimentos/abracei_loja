﻿<?php 

function wpbootstrap_scripts_with_jquery()
{
	// Register the script like this for a theme:
	wp_register_script( 'custom-script', get_template_directory_uri() . '/bootstrap/js/bootstrap.js', array( 'jquery' ) );
	// For either a plugin or a theme, you can then enqueue the script:
	wp_enqueue_script( 'custom-script' );
}
add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts_with_jquery' );


if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'id' => 'sidebar-1',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

function container_shortcode( $atts, $content = null ) {
	return '<div class="container">' . do_shortcode($content) . '</div>';
}

add_shortcode( 'bs_container', 'container_shortcode' );	


function facebook_curtir() {
	return '<div class="fb-page" data-href="https://www.facebook.com/abracei/" data-tabs="timeline" data-width="350" data-height="300" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/abracei/"><a href="https://www.facebook.com/abracei/">Abracei</a></blockquote></div></div>';
}
add_shortcode( 'facebook', 'facebook_curtir' );


function padding_shortcode( $atts, $content = null ) {
	return '<div class="caixa-padding">' . do_shortcode($content) . '</div>';
}
add_shortcode( 'caixa-padding', 'padding_shortcode' );	

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
}

// Registrar Widgets
function widgets_rodape() {

	$args = array(
		'id'            => 'footer',
		'name'          => __( 'Rodape', 'text_domain' ),
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
		'before_widget' => '<div class="col-md-3 rodape-widgets">',
		'after_widget'  => '</div>',
	);
	register_sidebar( $args );

}
add_action( 'widgets_init', 'widgets_rodape' );


// URLs relativas para Meta Slider
function metaslider_protocol_relative_urls($cropped_url, $orig_url) {
	return str_replace('http://', '//', $cropped_url);
}
add_filter('metaslider_resized_image_url', 'metaslider_protocol_relative_urls', 10, 2);


// MOVENDO ÁREAS WOOCOMMERCE

// Título antes das imagens
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
add_action( 'woocommerce_before_single_product_summary', 'woocommerce_template_single_title', 5 );

// Adicionar ao carrinho ao lado do preço
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 15 );

// Categoria e Tags no fim da página de produtos
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 900 );

// Mover descrição abaixo do frete
remove_action('woocommerce_single_product_summary','woocommerce_template_single_excerpt',20);
add_action('woocommerce_single_product_summary','woocommerce_template_single_excerpt',100);

// HOME
remove_action('woocommerce_before_main_content','woocommerce_breadcrumb',20);
add_action('woocommerce_before_main_content','woocommerce_slider',20);


// Alter produt loop individual products 
add_action( 'woocommerce_before_shop_loop_item_title', 'new_product_defaults_wrap_open' , 20 ); //opener
add_action( 'woocommerce_after_shop_loop_item_title', 'new_product_defaults_wrap_close', 40); //closer

function new_product_defaults_wrap_open() {
	echo '<div class="product-details">';
}
function new_product_defaults_wrap_close() {
	echo '</div><!--/.product-details-->';
}



if ( ! function_exists( 'woocommerce_content' ) ) {
	function woocommerce_content() {
		if ( has_term( 'destaque', 'product_cat' ) ) {
			while ( have_posts() ) : the_post();
				wc_get_template_part( 'content', 'single-product-destaque' );
			endwhile;
		}
		else {
			while ( have_posts() ) : the_post();
				wc_get_template_part( 'content', 'single-product' );
			endwhile;
		}
	}
}

// Menus
function registrar_menu_topo() {
  register_nav_menu('menu-topo',__( 'Menu Topo' ));
}
add_action( 'init', 'registrar_menu_topo' );

// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}


// Display 24 products per page. Goes in functions.php
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 9;' ), 20 );

// Altera ONSALE span para porcentagem de desconto */
add_filter('woocommerce_sale_flash', 'avia_change_sale_content', 10, 3);
function avia_change_sale_content($content, $post, $product){
	$cf = get_post_meta( get_the_ID(), 'desconto', true );
	if( ! empty( $cf ) ) {
		echo '<span class="onsale">'. $cf .' OFF</span>';
	}
}


/** Force URLs in srcset attributes into HTTPS scheme.* This is particularly useful when you're running a Flexible SSL frontend like Cloudflare*/function ssl_srcset( $sources ) {foreach ( $sources as &$source ) {$source['url'] = set_url_scheme( $source['url'], 'https' );}return $sources;}add_filter( 'wp_calculate_image_srcset', 'ssl_srcset' );

/* CSS Menu Walker */
class CSS_Menu_Maker_Walker extends Walker {

var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

function start_lvl( &$output, $depth = 0, $args = array() ) {
$indent = str_repeat("\t", $depth);
$output .= "\n$indent

\n";
}
function end_lvl( &$output, $depth = 0, $args = array() ) {
$indent = str_repeat("\t", $depth);
$output .= "$indent

\n";
}

function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

global $wp_query;
$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
$class_names = $value = '';
$classes = empty( $item->classes ) ? array() : (array) $item->classes;

/* Add active class */
if(in_array('current-menu-item', $classes)) {
$classes[] = 'active';
unset($classes['current-menu-item']);
}

/* Check for children */
$children = get_posts(array('post_type' => 'nav_menu_item', 'nopaging' => true, 'numberposts' => 1, 'meta_key' => '_menu_item_menu_item_parent', 'meta_value' => $item->ID));
if (!empty($children)) {
$classes[] = 'has-sub';
}

$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

$output .= $indent . '
';

$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';

$item_output = $args->before;
$item_output .= '';
$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
$item_output .= '';
$item_output .= $args->after;

$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
}

function end_el( &$output, $item, $depth = 0, $args = array() ) {
$output .= "

\n";
}
}

/**
* Set page url when cart is empty
*/
add_filter('wpmenucart_emptyurl', 'add_wpmenucart_emptyurl', 1, 1);
function add_wpmenucart_emptyurl ($empty_url) {
    $empty_url = '//abracei.com.br/carrinho/';
    return $empty_url;
}


/* CHECKOUT FERIAS */
/**
 * Add checkbox field to the checkout
 **/
add_action('woocommerce_after_order_notes', 'my_custom_checkout_field');
 
function my_custom_checkout_field( $checkout ) {
 
    echo '<div id="prazo-diferenciado">';
 
    woocommerce_form_field( 'my_checkbox', array(
        'type'          => 'checkbox',
        'class'         => array('input-checkbox'),
        'label'         => __('A Abracei estará em recesso do dia 22/12/2017 até o dia 07/01/2018. Ao finalizar esta compra, você concorda que o envio do seu produto será realizado a partir do dia 08/01/2018.'),
        'required'  => true,
        ), $checkout->get_value( 'my_checkbox' ));
 
    echo '</div>';
}

/**
 * Process the checkout
 **/
add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');
 
function my_custom_checkout_field_process() {
    global $woocommerce;
 
    // Check if set, if its not set add an error.
    if (!$_POST['my_checkbox'])
         $woocommerce->add_error( __('Você precisa concordar com a data diferenciada de envio durante o período de recesso.') );
}

/**
 * Update the order meta with field value
 **/
add_action('woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta');
 
function my_custom_checkout_field_update_order_meta( $order_id ) {
    if ($_POST['my_checkbox']) update_post_meta( $order_id, 'My Checkbox', esc_attr($_POST['my_checkbox']));
}



function admin_style() {
  wp_enqueue_style('admin-styles', get_template_directory_uri().'/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');
?>