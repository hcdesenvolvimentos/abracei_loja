      <footer>
		<div class="row footer">
			<?php if ( is_active_sidebar( 'footer' ) ) : ?>
				<div id="footer" role="complementary">
					<div class="container">
						<?php dynamic_sidebar( 'footer' ); ?>
					</div>
				</div><!-- #primary-sidebar -->
			<?php endif; ?>
		</div>
		<div class="row direitos">
			<div class="container">
			<p>© Abracei 2016 - Todos os Direitos Reservados | CNPJ: 14.679.588/0001-66</p>
			</div>
		</div>
      </footer>

    </div> <!-- /container -->
	</div>
	</div> <!-- /wrap -->
	<?php if ( current_user_can( 'manage_options' ) ) { } else { ?>
		
    <?php } wp_footer(); ?>
  </body>
</html>